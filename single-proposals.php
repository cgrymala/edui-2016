<?php
/**
 * The single proposal template file.
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
remove_all_actions( 'genesis_entry_content' );
add_action( 'genesis_entry_content', 'do_prop_template_content' );
?>
<style type="text/css">
	#evalform {
		padding:2em;
		background: #E3E3E3;
	}
	dl {border: 1px dashed #ccc; padding: 0.25em; margin:1em 0 1em .5em;}
	dt {float: left; clear: left; width:10em; text-align: right; font-weight: bold; color: #50142E; margin-right:.5em;} 
	dt:after { content: ":"; } 
	dd { margin: 0 0 0 2em; padding: 0 0 0.5em 0; }
</style>
<?php function do_prop_template_content() { 
	if(is_user_logged_in() ) : 
	$current_user = wp_get_current_user();
?> 		<?php 
       if (get_field('session_type')) {
			echo "<h3>". get_field('session_type') ."</h3>";			
		}	
        $posttags = get_the_tags();
		if ($posttags) {
			echo "<p class='post-meta'>Tags: ";
			foreach($posttags as $tag) {
				echo "<a href=\"". get_tag_link($tag->term_id) ."?post_type=proposals\">". $tag->name . '</a> '; 
			}
			echo "</p>";							  
		}	
		if (get_field('other_tags')) {
			echo "<p class='post-meta'><strong>Other Suggested Tags:</strong> ";
			echo get_field('other_tags');
			echo "</p>";
		}
	if (get_field('difficulty')) {
			echo "<p class='post-meta'><strong>Difficulty:</strong> ". get_field('difficulty') ."</p>";			
		}
	if (get_field('audience')) { ?>
			<p class='post-meta'><strong>This is for:</strong> <?php the_field('audience'); ?></p>
		<?php } 
	
		echo "<h3>Session Description</h3>";
       	the_content(); 
		if(get_field('attendees_will_learn')){									
			echo "<h3>Attendees Will Learn</h3><p>";
			the_field('attendees_will_learn');
			echo "</p>";
		}
		if(get_field('attendees_will_discuss')){									
		                  	echo "<h3>Attendees Will Discuss</h3><p>";
							the_field('attendees_will_discuss');
							echo "</p>";
							}
							if(get_field('activities')){									
		                  	echo "<h3>Mini-Workshop Activitites</h3><p>";
							the_field('activities');
							echo "</p>";
							}
							
							
							if(get_field('speaker_1_name')){									
		                  		echo "<h3 class='speaker'>Speaker</h3>";
								echo "<h4>". get_field('speaker_1_name')."</h4>";
								echo "<h5 class='organization'>". get_field('speaker_1_organization') ." - " . get_field('speaker_1_job_title')."</h5>";
								echo "<p>". get_field('speaker_1_bio') ."</p>";
								echo "<h4>Speaking Experience</h4>";
								echo "<p>". get_field('speaker_1_experience') ."</p>";
							}
							
							if(get_field('speaker_2_name')){									
		                  		echo "<h3>Co-Speaker</h3>";
								echo "<h4>". get_field('speaker_2_name')."</h4>";
								echo "<h5 class='organization'>". get_field('speaker_2_organization') ." - " . get_field('speaker_2_job_title')."</h5>";
								echo "<p>". get_field('speaker_2_bio') ."</p>";
								echo "<h4>Speaking Experience</h4>";
								echo "<p>". get_field('speaker_2_experience') ."</p>";
							}
							if(get_field('speaker_3_name')){									
		                  		echo "<h3>Co-Speaker</h3>";
								echo "<h4>". get_field('speaker_3_name')."</h4>";
								echo "<h5 class='organization'>". get_field('speaker_3_organization') ." - " . get_field('speaker_3_job_title')."</h5>";
								echo "<p>". get_field('speaker_3_bio') ."</p>";
								echo "<h4>Speaking Experience</h4>";
								echo "<p>". get_field('speaker_3_experience') ."</p>";
							}							
							if(get_field('proposal_notes')){									
		                  	echo "<h3>Additional Notes</h3><p>";
							the_field('proposal_notes');
							echo "</p>";
							}
	
			echo "<div id='evalform' class='evaluation'> ";                                
                       
						if (get_field('proposal_summary_mode', 'option')==false){
							$myEval = getEval(get_the_ID(), $current_user->user_login, 22, true);							
					   		$theEval = getEvalValues(get_the_ID(), $current_user->user_login, 22);
							if ($myEval){  
								echo("<h3 class='gform_title' style='font-weight:bold'>Your score</h3>". $myEval. " ");  
							} 
							gravity_form('22', $display_title=true, $display_description=true, $display_inactive=false, $field_values=null, $ajax=true, $tabindex); 
							
						} else {
							if (get_field('average_score')){
					   		echo "<h2>Summary</h2>";
									echo "<dl>";
									if(get_field('average_score')){
										echo "<dt>Average Score</dt>";
										echo "<dd>". get_field ('average_score') ."</dd>";
									}
									if(get_field('rating_count')){
										echo "<dt>Rated by</dt>";
										echo "<dd>". get_field ('rating_count') ." Team Members</dd>";
									}
									if(get_field('high_score')){
										echo "<dt>High Score</dt>";
										echo "<dd>". get_field ('high_score') ."</dd>";
									}
									if(get_field('low_score')){
										echo "<dt>Low Score</dt>";
										echo "<dd>". get_field ('low_score') ."</dd>";
									}
									if(get_field('application_status')){
										echo "<dt>Decision</dt>";
										echo "<dd>". get_field ('application_status') ."</dd>";
									} else {
										echo "<dt>Decision</dt>";
										echo "<dd>Not Discussed</dd>";	
									}
									echo "</dl>";																		
								}
								echo "<h2>Evaluations</h2>";
								echo getAllEvals(get_the_ID(), '22');
						}
			echo "</div>";
		?>
							
<?php
	endif; 
}
genesis();
?>