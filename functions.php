<?php
/**
 * The main functions declaration file for the edUI 2016 WordPress theme
 * @version 0.1
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

if ( ! class_exists( 'edui2016' ) ) {
	/**
	 * The main class that holds all of the theme functions & features
	 */
	class edui2016 {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $version = '0.1.10';
		
		/**
		 * Holds the class instance.
		 *
		 * @since	1.0.0
		 * @access	private
		 * @var		edui2016
		 */
		private static $instance;
		
		/**
		 * Placeholder to know whether we are manipulating a custom post field value
		 * 
		 * @since  0.1
		 * @access public
		 * @var    bool
		 */
		public $doing_meta_filter = false;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   0.1
		 * @return	edui2016
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the edui2016 object
		 */
		function __construct() {
			/**
			 * Make sure the theme is set up as a responsive HTML5 theme
			 */
			add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'nav',
				'subnav',
				'site-inner',
				'footer-widgets',
				'footer'
			) );
			//* Add support for custom header
			add_theme_support( 'custom-header', array(
				'flex-height'     => true,
				'width'           => 196,
				'height'          => 50,
				'header-selector' => '.site-title a',
				'header-text'     => false,
			) );
			
			/**
			 * Remove the parent theme style sheet & set up enqueues for our custom styles
			 */
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
			
			/**
			 * Begin setting up the theme and its features
			 */
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			
			add_image_size( 'home-post-feature', 550, 275, true );
			add_image_size( 'home-venue', 600, 425, true );
			add_image_size( 'home-marquee', 1300, 800, true );
			add_image_size( 'featured-speakers', 300, 300, true );
			
			add_shortcode( 'pull-quote', array( $this, 'do_pull_quote' ) );
			add_shortcode( 'first', array( $this, 'do_first' ) );
			add_shortcode( 'wpv-date', array( $this, 'do_date_shortcode' ) );
			add_shortcode( 'wpv-minute-blocks', array( $this, 'do_time_blocks' ) );
			
			/**
			 * Try to make ACF relationship fields compatible with Views
			 */
			add_filter( 'get_post_metadata', array( $this, 'get_acf_metadata' ), 10, 4 );
			
			add_action( 'pre_get_posts', array( $this, 'sort_archives' ) );
			
			/**
			 * Fix VFH logo link
			 */
			add_filter( 'home_url', array( $this, 'vfh_logo_link' ) );
			
			global $skip_acf_data_massage;
			$skip_acf_data_massage = false;
		}
		
		/**
		 * Set up our theme style sheets
		 */
		function enqueue_styles() {
			wp_register_style( 'genesis', get_stylesheet_uri(), array(), $this->version, 'all' );
			wp_enqueue_style( 'edui-2016', get_bloginfo( 'stylesheet_directory' ) . '/edui.css', array( 'genesis' ), $this->version, 'all' );
		}
		
		/**
		 * Register any custom sidebars/widget areas this theme needs
		 */
		function register_sidebars() {
			genesis_register_sidebar( array( 
				'id' => 'pre-footer', 
				'name' => __( 'Pre Footer' ), 
				'description' => __( 'Appears above the the footer; full-width' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'full-footer', 
				'name' => __( 'Footer Full-Width' ), 
				'description' => __( 'Appears at the top of the footer; full-width' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'below-footer-1', 
				'name' => __( 'Footer 1' ), 
				'description' => __( 'Appears at the bottom of the footer; two-thirds width on the left' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'below-footer-2', 
				'name' => __( 'Footer 2' ), 
				'description' => __( 'Appears at the bottom of the footer; one-third width on the right' ) 
			) );
			
			/*genesis_register_sidebar( array( 
				'id' => 'front-feature', 
				'name' => __( '[Front Page] Main Feature' ), 
				'description' => __( 'Appears at the very top of the home page; useful for a slider or hero image' ) 
			) );*/
			genesis_register_sidebar( array( 
				'id' => 'front-widgets-1', 
				'name' => __( '[Front Page] Widget Area 1' ), 
				'description' => __( 'Appears below the front page main feature' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'front-widgets-2', 
				'name' => __( '[Front Page] Widget Area 2' ), 
				'description' => __( 'Appears below the first front page widget area' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'front-widgets-3', 
				'name' => __( '[Front Page] Widget Area 3' ), 
				'description' => __( 'Appears below the second front page widget area' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'front-widgets-4', 
				'name' => __( '[Front Page] Widget Area 4' ), 
				'description' => __( 'Appears below the third front page widget area' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'front-widgets-5', 
				'name' => __( '[Front Page] Widget Area 5' ), 
				'description' => __( 'Appears below the fourth front page widget area' ) 
			) );
			genesis_register_sidebar( array( 
				'id' => 'front-widgets-6', 
				'name' => __( '[Front Page] Widget Area 6' ), 
				'description' => __( 'Appears below the fifth front page widget area' ) 
			) );
		}
		
		/**
		 * Perform any changes to Genesis core features we need to handle
		 */
		function adjust_genesis() {
			remove_all_actions( 'genesis_footer' );
			add_action( 'genesis_footer', array( $this, 'do_footer' ) );
			
			add_action( 'genesis_after_header', array( $this, 'do_featured_image' ) );
			
			/**
			 * Unregister all of the alternative layouts, and force the full-width content layout
			 */
			$layouts = array( 'content-sidebar', 'sidebar-content', 'sidebar-content-sidebar', 'sidebar-sidebar-content', 'content-sidebar-sidebar' );
			foreach ( $layouts as $l ) {
				genesis_unregister_layout( $l );
			}
			add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
		}
		
		/**
		 * Handle any custom templates/loops we need on conditional bases
		 */
		function template_redirect() {
			if ( is_front_page() ) {
				add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
				remove_all_actions( 'genesis_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page' ) );
			}
			if ( is_singular( 'speakers' ) || is_singular( 'sessions' ) ) {
				remove_all_actions( 'genesis_entry_header' );
				remove_action( 'genesis_after_header', array( $this, 'do_featured_image' ) );
				remove_all_actions( 'genesis_entry_footer' );
			}
			/*if ( is_page( 'another-schedule-test' ) ) {
				remove_all_actions( 'genesis_loop' );
				add_action( 'genesis_loop', array( $this, 'do_schedule_loop' ) );
			}*/
		}
		
		/**
		 * Output the widgetized front page
		 */
		function do_front_page() {
			if ( has_post_thumbnail() ) {
				$imageid = get_post_thumbnail_id();
				$image = wp_get_attachment_image_src( $imageid, 'home-marquee', false );
				if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
	<aside class="home-main-feature" style="background-image: url( <?php echo $image[0] ?> );">
    	<div class="wrap">
            <div class="home-feature-wrap">
                <div class="home-feature-content">
                	<div class="home-feature-text">
	                    <?php the_content() ?>
                    </div>
                </div>
            </div>
        </div>
    </aside>

<?php
					endwhile;
				else : 
?>
	<aside class="home-main-feature">
    	<h1>No front page was retrieved for some reason</h1>
    </aside>
<?php
				endif;
			}
			
			foreach ( array( /*'front-feature', */'front-widgets-1', 'front-widgets-2', 'front-widgets-3', 'front-widgets-4', 'front-widgets-5', 'front-widgets-6' ) as $s ) {
				if ( is_active_sidebar( $s ) ) {
					printf( '<aside class="widget-area %s"><div class="wrap">', ( stristr( $s, 'front-widgets' ) ? $s . ' front-widgets' : $s ) );
					dynamic_sidebar( $s );
					echo '</div></aside>';
				}
			}
		}
		
		/**
		 * Insert the featured image as a hero image at the top of an 
		 * 		individual page/post
		 */
		function do_featured_image() {
			if ( ! is_singular() || ! get_field( 'banner_image' ) || is_front_page() ) {
				return;
			}
			
			$bannerimage = get_field( 'banner_image' );
			if ( is_array( $bannerimage ) && array_key_exists( 'url', $bannerimage ) ) {
				if ( array_key_exists( 'home-marquee', $bannerimage['sizes'] ) ) {
					$bannerimage = $bannerimage['sizes']['home-marquee'];
				} else {
					$bannerimage = $bannerimage['url'];
				}
			} else {
				return;
			}
			
			remove_all_actions( 'genesis_entry_header' );
?>
	<aside class="post-main-feature" style="background-image: url( <?php echo $bannerimage ?> );">
    	<div class="post-feature-wrap">
        	<h1 class="post-feature-content">
            	<?php the_title() ?>
            </h1>
        </div>
    </aside>
<?php
		}
		
		/**
		 * Output the widgetized footer
		 */
		function do_footer() {
?>
<div class="edui-footer">
	<div class="wrap">
    <?php
			if ( is_active_sidebar( 'pre-footer' ) ) {
?>
    <aside class="pre-footer"><div class="wrap">
<?php
				dynamic_sidebar( 'pre-footer' );
?>
		</div></aside>
<?php
			}
?>            
<?php
			if ( is_active_sidebar( 'full-footer' ) ) {
?>
		<aside class="full-footer">
        	<div class="wrap">
<?php
				dynamic_sidebar( 'full-footer' );
?>
			</div>
		</aside>
<?php
			}
?>
        <div class="bottom-footer-widgets clearfix">
        	<div class="wrap">
<?php
			if ( is_active_sidebar( 'below-footer-1' ) && is_active_sidebar( 'below-footer-2' ) ) {
?>
                <aside class="footer-1-widget-area two-thirds first">
                    <?php dynamic_sidebar( 'footer-1' ) ?>
                </aside>
                <aside class="footer-2-widget-area one-third">
                    <?php dynamic_sidebar( 'footer-2' ) ?>
                </aside>
<?php
			} else if ( is_active_sidebar( 'below-footer-1' ) ) {
?>
                <aside class="footer-1-widget-area">
<?php
				dynamic_sidebar( 'below-footer-1' );
?>
                </aside>
<?php
			} else if ( is_active_sidebar( 'below-footer-2' ) ) {
?>
                <aside class="footer-2-widget-area">
<?php
				dynamic_sidebar( 'below-footer-2' );
?>
                </aside>
<?php
			}
?>
			</div>
	    </div>
	</div>
</div>
<?php
		}
		
		function do_pull_quote( $attrs=null, $content=null ) {
			return sprintf( '<aside class="pull-quote">%s</aside>', $content );
		}
		
		function do_first( $atts=null, $content=null ) {
			return sprintf( '<p class="first-paragraph" data-first-letter="%s">%s</p>', substr( strip_tags( $content ), 0, 1 ), $content );
		}
		
		function get_acf_metadata( $na, $object_id, $meta_key, $single ) {
			global $skip_acf_data_massage;
			if ( is_admin() )
				return null;
			if ( 'speakers' != $meta_key && 'speaker-presentations' != $meta_key )
				return null;
			if ( true === $this->doing_meta_filter || true === $skip_acf_data_massage ) {
				$this->doing_meta_filter = false;
				return null;
			}
			
			$this->doing_meta_filter = true;
			if ( 'speakers' == $meta_key ) {
				$vals = maybe_unserialize( get_post_meta( $object_id, $meta_key, true ) );
			} else if ( 'speaker-presentations' == $meta_key ) {
				$posts = get_posts( array(
					'post_type' => 'sessions', 
					'meta_query' => array( array( 
						'key' => 'speakers', 
						'value' => '"' . $object_id . '"', 
						'compare' => 'LIKE', 
					) )
				) );
				$vals = array();
				foreach ( $posts as $p ) {
					$vals[] = $p->ID;
				}
			}
			$this->doing_meta_filter = false;
			return $vals;
		}
		
		function sort_archives() {
			if ( is_admin() )
				return;
			
			global $wp_query;
			if ( is_post_type_archive( 'speakers' ) || is_post_type_archive( 'sessions' ) ) {
				if ( is_post_type_archive( 'sessions' ) ) {
					set_query_var( 'orderby', 'meta_value_num' );
					set_query_var( 'meta_key', 'date-time' );
				} else {
					set_query_var( 'orderby', 'title' );
				}
				set_query_var( 'order', 'ASC' );
				set_query_var( 'posts_per_page', -1 );
			}
		}
		
		function vfh_logo_link( $url ) {
			if ( ! doing_action( 'genesis_site_title' ) )
				return $url;
			if ( is_admin() )
				return $url;
			
			return apply_filters( 'edui_vfh_logo_url', 'http://virginiahumanities.org/' );
		}
		
		function do_date_shortcode( $atts=array() ) {
			$atts = shortcode_atts( array(
				'field'      => null, 
				'format'     => get_option( 'date_format' ), 
				'id'         => get_the_ID(), 
				'addminutes' => 0, 
			), $atts );
			
			$val = get_post_meta( $atts['id'], $atts['field'], true );
			$val = maybe_unserialize( $val );
			if ( ! is_numeric( $val ) )
				return '';
			
			if ( ! empty( $atts['addminutes'] ) && is_numeric( $atts['addminutes'] ) )
				$val = ( $val * 1 ) + ( $atts['addminutes'] * 60 );
			
			return date( $atts['format'], $val );
		}
		
		function do_time_blocks( $atts=array() ) {
			$atts = shortcode_atts( array(
				'date' => null, 
				'step' => 5
			), $atts );
			
			$minutes = range( 0, 1440, $atts['step'] );
			$blocks = array();
			$day = strtotime( $atts['date'] );
			foreach ( $minutes as $m ) {
				$blocks[] = ( $day + $m );
			}
			
			return $blocks;
		}
		
		function do_schedule_loop() {
			$this->latest_workshop = $this->latest_conference_block = $this->latest_session = array( 'start' => 0, 'end' => 0 );
			$args = array(
				'post_type'      => 'sessions', 
				'posts_per_page' => -1, 
				'post_status'    => 'publish', 
			);
			$sessions = array();
			$q = get_posts( $args );
			foreach ( $q as $p ) {
				$time = get_field( 'date-time', $p->ID, false );
				$location = get_field( 'location', $p->ID );
				if ( empty( $location ) || empty( $time ) )
					continue;
				$date = date( 'Y-m-d', $time );
				switch ( $date ) {
					case '2016-10-24' : 
						$day = 1;
						break;
					case '2016-10-25' : 
						$day = 2;
						break;
					case '2016-10-26' : 
						$day = 3;
						break;
					default : 
						$day = 4;
						break;
				}
				$sessions[$day][$time][$location] = clone $p;
			}
			
			ksort( $sessions );
			/*print( '<pre><code>' );
			var_dump( $sessions );
			print( '</code></pre>' );*/
			
			foreach ( $sessions as $day => $timedata ) {
				echo '<div class="schedule-day day-' . $day . ' clearfix">';
				switch ( $day ) {
					case 1 : 
						$dayname = 'Monday, October 24, 2016';
						break;
					case 2 : 
						$dayname = 'Tuesday, October 25, 2016';
						break;
					case 3 : 
						$dayname = 'Wednesday, October 26, 2016';
						break;
					default : 
						$dayname = 'Date and Time TBD';
						break;
				}
				printf( '<header><h1>%s</h1></header>', $dayname );
				ksort( $timedata );
				foreach ( $timedata as $time => $locdata ) {
					ksort( $locdata );
					switch ( count( $locdata ) ) {
						case 1 :
							$tmp = array_keys( $locdata );
							if ( 'CitySpace' == $tmp[0] ) {
								$this->do_session_blocks( $locdata );
							} else {
								$this->do_conference_wide_block( $locdata );
							}
							break;
						default : 
							$this->do_session_blocks( $locdata );
							break;
					}
				}
				echo '</div>';
			}
		}
		
		function do_conference_wide_block( $sessions=array() ) {
			foreach ( $sessions as $location => $session ) {
				$date = get_field( 'date-time', $session->ID, false );
				$duration = get_field( 'duration', $session->ID, false );
				$end = ( $date + ( $duration * 60 ) );
				$type = get_the_terms( $session, 'session-type' );
				if ( is_array( $type ) )
					$type = array_shift( $type );
				if ( ! is_object( $type ) )
					$type = false;
				$short_title = get_field( 'short-title', $session->ID );
				if ( empty( $short_title ) )
					$short_title = $session->post_title;
				
				$sessioninfo = sprintf( '<header><h2>%1$s</h2><h3>%2$s</h3></header><p class="session-date">%3$s</p><p class="session-location">%4$s</p>', '<strong>' . $type->name . ':</strong> ' . $session->post_title, $short_title, date( 'F j, Y g:i a', $date ) . ' - ' . date( 'g:i a', $end ), $location );
				
				printf( '<a href="%7$s"><article class="conference-wide %1$s %6$s clearfix" data-duration="%2$d" data-start="%3$s" data-end="%4$s">%5$s</article></a>', sanitize_title( $location ), $duration, $date, $end, $sessioninfo, $type->slug, get_permalink( $session->ID ) );
				
				$this->latest_conference_block = array( 'start' => $date, 'end' => $end );
			}
		}
		
		function do_session_blocks( $sessions=array() ) {
			$wrapper = '<div class="%2$s">%1$s</div>';
			$classes = 'session-block';
			$meat = '';
			
			if ( ! array_key_exists( 'CitySpace', $sessions ) ) {
				$has_workshop = false;
			} else {
				$no_workshop = true;
			}
			
			foreach ( $sessions as $location => $session ) {
				$date = get_field( 'date-time', $session->ID, false );
				$duration = get_field( 'duration', $session->ID, false );
				$end = ( $date + ( $duration * 60 ) );
				$type = get_the_terms( $session, 'session-type' );
				if ( is_array( $type ) )
					$type = array_shift( $type );
				if ( ! is_object( $type ) )
					$type = false;
				$short_title = get_field( 'short-title', $session->ID );
				if ( empty( $short_title ) )
					$short_title = $session->post_title;
				
				switch ( $location ) {
					case 'CitySpace' : 
						$this->latest_workshop = array( 'start' => $date, 'end' => $end );
						break;
					default : 
						$this->latest_session = array( 'start' => $date, 'end' => $end );
						break;
				}
				
				$sessioninfo = sprintf( '<header><h2>%1$s</h2><h3>%2$s</h3></header><p class="session-date">%3$s</p><p class="session-location">%4$s</p>', '<strong>' . $type->name . ':</strong> ' . $session->post_title, $short_title, date( 'F j, Y g:i a', $date ) . ' - ' . date( 'g:i a', $end ), $location );
				
				$meat .= sprintf( '<a href="%7$s"><article class="one-fourth %1$s %6$s" data-duration="%2$d" data-start="%3$s" data-end="%4$s">%5$s</article></a>', sanitize_title( $location ) . ( 'CitySpace' == $location ? ' first' : '' ), $duration, $date, $end, $sessioninfo, $type->slug, get_permalink( $session->ID ) );
			}
			
			if ( false === $has_workshop ) {
				if ( $this->latest_workshop['end'] < $this->latest_session['start'] ) {
					$meat = sprintf( '<article class="one-fourth first break">&nbsp;</article>%s', $meat );
					$classes .= ' clearfix';
				}
			}
			
			printf( $wrapper, $meat, $classes );
		}
	}
}

/**
* Limit How Many Checkboxes Can Be Checked
* http://gravitywiz.com/2012/06/11/limiting-how-many-checkboxes-can-be-checked/
*/

class GFLimitCheckboxes {

    private $form_id;
    private $field_limits;
    private $output_script;

    function __construct($form_id, $field_limits) {

        $this->form_id = $form_id;
        $this->field_limits = $this->set_field_limits($field_limits);

        add_filter("gform_pre_render_$form_id", array(&$this, 'pre_render'));
        add_filter("gform_validation_$form_id", array(&$this, 'validate'));

    }

    function pre_render($form) {

        $script = '';
        $output_script = false;

        foreach($form['fields'] as $field) {

            $field_id = $field['id'];
            $field_limits = $this->get_field_limits($field['id']);

            if( !$field_limits                                          // if field limits not provided for this field
                || RGFormsModel::get_input_type($field) != 'checkbox'   // or if this field is not a checkbox
                || !isset($field_limits['max'])        // or if 'max' is not set for this field
                )
                continue;

            $output_script = true;
            $max = $field_limits['max'];
            $selectors = array();

            foreach($field_limits['field'] as $checkbox_field) {
                $selectors[] = "#field_{$form['id']}_{$checkbox_field} .gfield_checkbox input:checkbox";
            }

            $script .= "jQuery(\"" . implode(', ', $selectors) . "\").checkboxLimit({$max});";

        }

        GFFormDisplay::add_init_script($form['id'], 'limit_checkboxes', GFFormDisplay::ON_PAGE_RENDER, $script);

        if($output_script):
            ?>

            <script type="text/javascript">
            jQuery(document).ready(function($) {
                $.fn.checkboxLimit = function(n) {

                    var checkboxes = this;

                    this.toggleDisable = function() {

                        // if we have reached or exceeded the limit, disable all other checkboxes
                        if(this.filter(':checked').length >= n) {
                            var unchecked = this.not(':checked');
                            unchecked.prop('disabled', true);
                        }
                        // if we are below the limit, make sure all checkboxes are available
                        else {
                            this.prop('disabled', false);
                        }

                    }

                    // when form is rendered, toggle disable
                    checkboxes.bind('gform_post_render', checkboxes.toggleDisable());

                    // when checkbox is clicked, toggle disable
                    checkboxes.click(function(event) {

                        checkboxes.toggleDisable();

                        // if we are equal to or below the limit, the field should be checked
                        return checkboxes.filter(':checked').length <= n;
                    });

                }
            });
            </script>

            <?php
        endif;

        return $form;
    }

    function validate($validation_result) {

        $form = $validation_result['form'];
        $checkbox_counts = array();

        // loop through and get counts on all checkbox fields (just to keep things simple)
        foreach($form['fields'] as $field) {

            if( RGFormsModel::get_input_type($field) != 'checkbox' )
                continue;

            $field_id = $field['id'];
            $count = 0;

            foreach($_POST as $key => $value) {
                if(strpos($key, "input_{$field['id']}_") !== false)
                    $count++;
            }

            $checkbox_counts[$field_id] = $count;

        }

        // loop through again and actually validate
        foreach($form['fields'] as &$field) {

            if(!$this->should_field_be_validated($form, $field))
                continue;

            $field_id = $field['id'];
            $field_limits = $this->get_field_limits($field_id);

            $min = isset($field_limits['min']) ? $field_limits['min'] : false;
            $max = isset($field_limits['max']) ? $field_limits['max'] : false;

            $count = 0;
            foreach($field_limits['field'] as $checkbox_field) {
                $count += rgar($checkbox_counts, $checkbox_field);
            }

            if($count < $min) {
                $field['failed_validation'] = true;
                $field['validation_message'] = sprintf( _n('You must select at least %s item.', 'You must select at least %s items.', $min), $min );
                $validation_result['is_valid'] = false;
            }
            else if($count > $max) {
                $field['failed_validation'] = true;
                $field['validation_message'] = sprintf( _n('You may only select %s item.', 'You may only select %s items.', $max), $max );
                $validation_result['is_valid'] = false;
            }

        }

        $validation_result['form'] = $form;

        return $validation_result;
    }

    function should_field_be_validated($form, $field) {

        if( $field['pageNumber'] != GFFormDisplay::get_source_page( $form['id'] ) )
    		return false;

        // if no limits provided for this field
        if( !$this->get_field_limits($field['id']) )
            return false;

        // or if this field is not a checkbox
        if( RGFormsModel::get_input_type($field) != 'checkbox' )
            return false;

        // or if this field is hidden
        if( RGFormsModel::is_field_hidden($form, $field, array()) )
            return false;

        return true;
    }

    function get_field_limits($field_id) {

        foreach($this->field_limits as $key => $options) {
            if(in_array($field_id, $options['field']))
                return $options;
        }

        return false;
    }

    function set_field_limits($field_limits) {

        foreach($field_limits as $key => &$options) {

            if(isset($options['field'])) {
                $ids = is_array($options['field']) ? $options['field'] : array($options['field']);
            } else {
                $ids = array($key);
            }

            $options['field'] = $ids;

        }

        return $field_limits;
    }

}

new GFLimitCheckboxes(40, array(
    6 => array(
        'min' => 0, 
        'max' => 3
        )
    ));
/**
 * Instantiate an instance of the edui2016 class
 */
function edui2016() {
	return edui2016::instance();
}
// allow public to us GF Post Edit
add_filter('gform_update_post/public_edit', '__return_true');

// Gravity Forms Functions for Proposal Eval
add_action("gform_after_submission_22", "process_eval", 10, 2);
function process_eval($entry, $form){
	$form_id = $form["id"];
	foreach ($form["fields"] as $field) {
		if (strtolower($field["label"]) == "unique"){
                $uniqueID = $field["id"];	
				$evalID = $entry[$uniqueID];					
		} else if (strtolower($field["label"]) == "proposal id"){
			$propFieldID = $field["id"];	
			$proposalID = $entry[$propFieldID];
		} else if (strtolower($field["label"]) == "accept or reject?"){
			$ratingFieldID = $field["id"];	
			$rating = $entry[$ratingFieldID];
		}
	}
	
	$search_criteria["field_filters"][] = array('key' => $uniqueID, 'value' => $evalID);	
	$all_entries = GFAPI::get_entries($form_id, $search_criteria);
	foreach ($all_entries as $existing_entry){
		if($entry[$uniqueID]==$evalID && $entry["id"]!=$existing_entry["id"]){
			$success = GFAPI::delete_entry($existing_entry["id"]);
		}
	}
	avg_ratings($form, $ratingFieldID, $propFieldID, $proposalID);
	
		
}

function avg_ratings($form, $ratingFieldID, $propFieldID, $proposalID){
	$form_id = $form["id"];
	$search_criteria["field_filters"][] = array('key' => $propFieldID, 'value' => $proposalID);	
	$all_evals = GFAPI::get_entries($form_id, $search_criteria);
	$avgRating = 0;
	$highScore = 0;
	$lowScore = 10;
	$totRating = 0;
	$countRatings = 0;
	foreach ($all_evals as $this_eval){
		$rating = $this_eval[$ratingFieldID];
		$totRating = $totRating + $rating;
		if($rating >= $highScore){
			$highScore = $rating;
		}
		if($rating <= $lowScore){
			$lowScore = $rating;
		}
		$countRatings = $countRatings + 1;
	}
	$avgRating = $totRating/$countRatings;
	$avgRating = round($avgRating , 1);
	$field_key = "field_5318c3d04eaad"; //Average Score	
	update_field($field_key, $avgRating, $proposalID);
	$field_key = "field_5346b2cab2a20"; //Rating Count
	update_field($field_key, $countRatings, $proposalID);
	$field_key = "field_5346b4f558202"; //High Score
	update_field($field_key, $highScore, $proposalID);
	$field_key = "field_5346b50658203"; //Low Score
	update_field($field_key, $lowScore, $proposalID);
	
	
}
function getEvalValues($id, $user, $form_id){
	$form = GFAPI::get_form($form_id);
	foreach ($form["fields"] as $field) {
		if (strtolower($field["label"]) == "unique"){
             $uniqueFieldID = $field["id"];			    					
		} else if (strtolower($field["label"]) == "accept or reject?"){
			$ratingFieldID = $field["id"];				
		} else if (strtolower($field["label"]) == "comments"){
			$commentFieldID = $field["id"];				
		}

	}
	$uniqueID = $id."-".$user;
	$evalText = array(0, "Definitely pass", "Probably pass", "Probably accept", "Definitely accept");
	
	$search_criteria["field_filters"][] = array('key' => $uniqueFieldID, 'value' => $uniqueID);	
	$entries = GFAPI::get_entries($form_id, $search_criteria);
	$evalID = $entries[0]["id"];
	if($evalID){
		$entry = GFAPI::get_entry($evalID);
		$myRating = $entry[$ratingFieldID];
		//$myRating = $evalText[$myRating];
		$myComment = $entry[$commentFieldID];
		
		$output = array("rating"=>$myRating, "comment"=>$myComment);
		return $output;
	} else {
		return false;
	}
		
}
function getAllEvals($id, $form_id){
	$form = GFAPI::get_form($form_id);
	foreach ($form["fields"] as $field) {
		if (strtolower($field["label"]) == "proposal id"){
             $proposalIDFieldID = $field["id"];	
		} else if (strtolower($field["label"]) == "accept or reject?"){
			$ratingFieldID = $field["id"];				
		} else if (strtolower($field["label"]) == "comments"){
			$commentFieldID = $field["id"];				
		} else if (strtolower($field["label"]) == "unique"){
             $uniqueFieldID = $field["id"];				 	
		}

	}
	
	$evalText = array(0, "Definitely pass", "Probably pass", "Probably accept", "Definitely accept");
	$search_criteria["field_filters"][] = array('key' => $proposalIDFieldID, 'value' => $id);	
	$entries = GFAPI::get_entries($form_id, $search_criteria);
	$output = "<ul>";
	foreach ($entries as $entry) {
		$commenter = $entry[$uniqueFieldID];
		$temp = explode("-", $commenter);
		$commenter = $temp[1];
		$rating = $entry[$ratingFieldID];
		$rating = $evalText[$rating];
		$comment = $entry[$commentFieldID];
		
		$output .= "<li><strong>". $commenter. ": </strong>". $rating ;
		if($comment){
			$output .= "<br>". $comment;
		}
		$output .= "</li>";	
	}
	$output .= "</ul>";
	return $output;
		
}

function getEval($id, $user, $form_id, $comment){
	
	$form = GFAPI::get_form($form_id);
	foreach ($form["fields"] as $field) {
		if (strtolower($field["label"]) == "unique"){
             $uniqueFieldID = $field["id"];			    					
		} else if (strtolower($field["label"]) == "accept or reject?"){
			$ratingFieldID = $field["id"];				
		} else if (strtolower($field["label"]) == "comments"){
			$commentFieldID = $field["id"];				
		}

	}
	$uniqueID = $id."-".$user;
	$evalText = array(0, "Definitely pass", "Probably pass", "Probably accept", "Definitely accept");
	
	$search_criteria["field_filters"][] = array('key' => $uniqueFieldID, 'value' => $uniqueID);	
	$entries = GFAPI::get_entries($form_id, $search_criteria);
	$evalID = $entries[0]["id"];
	if($evalID){
		$output = "<p>";
		$entry = GFAPI::get_entry($evalID);
		$myRating = $entry[$ratingFieldID];
		$myRating = $evalText[$myRating];
		$output = "Rating: ".$myRating."<br>";
		
		if($comment != false){	
			$myComment = $entry[$commentFieldID];
			$output .= "Comment: ".$myComment;
		}
		$output .= "</p>";
		
	} else {
		$output = false;
	}
	return $output;	
	
}

// Shortcode to retrieve Current User's Proposal Rating //
function get_rating($atts) {
	extract(shortcode_atts(array(
		  'postid' => 1, 
	   ), $atts));
	$formid = get_field('prop_rev_gf_id', 'option');
	$current_user = wp_get_current_user();
	$current_user = $current_user->user_login;		

	if (get_field('proposal_summary_mode', 'option')==false){				
		$eval = getEval($postid, $current_user, $formid, false);
		if($eval){
			return $eval;
		} else {
			return "Not Rated";
		}
	} else {
		$avgScore = get_field('average_score', $postid);
		$highScore = get_field('high_score', $postid);
		$lowScore = get_field('low_score', $postid);
		$scoreCount = get_field('rating_count', $postid);
		
		$output = "Average Score: " . $avgScore . "<br>High/Low: " . $highScore . "/" . $lowScore . "<br>Rated by: " . $scoreCount . " reviewers";
		//$output = "Average Score: " . $avgScore;
	
		
		return $output;
	}
}
add_shortcode('getrating', 'get_rating');

add_filter( 'embed_defaults', 'change_embed_size' );

function change_embed_size() {
    // Adjust values
    return array('width' => 800, 'height' => 450);
}
edui2016();