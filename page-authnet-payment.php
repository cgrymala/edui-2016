<?php
/**
 * Template Name: Authorize.Net Payment
 *
 * Selectable from a dropdown menu on the edit page screen.
 */

remove_all_actions( 'genesis_entry_content' );
add_action( 'genesis_entry_content', 'do_authnet_template_content' );
add_action( 'genesis_before_header', 'do_authnet_head' );
?>
<?php 
function do_authnet_head() {
	//check to see if there's a payment response code
	if($_POST["x_response_code"]){ 
		$ResponseCode = trim($_POST["x_response_code"]);
		$TransID = trim($_POST["x_trans_id"]);
		$CustID = trim($_POST["x_cust_id"]);
		$Amount = trim($_POST["x_amount"]);
		echo("<script type='text/javascript'>window.location='http://eduiconf.org/vfh-payment-page/?response=". $ResponseCode ."&custid=". $CustID ."&transid=". $TransID. "&amount=". $Amount ."';</script>");
		exit();
	}
}

function do_authnet_template_content() {
	require_once get_stylesheet_directory() . '/anet_php_sdk/AuthorizeNet.php'; // Include the Authorize.net SDK 
	require_once get_stylesheet_directory() . '/anet_php_sdk/edui-api-credentials.php'; // Include the file where the API constants are defined
	
	$api_login_id = AUTH_NET_API_LOGIN_ID;
	$transaction_key = AUTH_NET_TRANSACTION_KEY;
	
	$fp_timestamp = time();
	$fp_sequence = "VFH" . time(); // Enter an invoice or other unique number.
	
	//Form submitted, but payment not made yet. Display handoff form to Authorize.net
	if($_GET['entry_id']){			
		$entryID = $_GET['entry_id'];			
		$formEntry = retrieve_gf_entry($entryID);			
		$amount = $formEntry["Total"];
		$fingerprint = AuthorizeNetSIM_Form::getFingerprint($api_login_id, $transaction_key, $amount, $fp_sequence, $fp_timestamp);	
		echo "<p>";
		if ($formEntry["Description"]){
			echo ("<h3>" . $formEntry["Description"] . "</h3>");
		}
		if ($formEntry["First Name"]){
			echo ("Name: " . $formEntry["First Name"] . " ". $formEntry["Last Name"]."<br/>");
		}
		if ($formEntry["Email"]){
			echo ("Email: ". $formEntry["Email"]."<br/>");
		}
		if ($formEntry["Total"]){
			echo ("<br/><strong>Total: $". $formEntry["Total"]."</strong><br/>");
		}			
		
		echo "</p>";
		if($_GET["cancel"] && $_GET["entry_id"]){
			GFAPI::delete_entry($_GET["entry_id"]);	
			echo "<p style='color:#9D1837; font-weight:bold;'>Your order has been cancelled.</p>";	
		} else {		
		?>
        <p>You will now be taken to a secure form to process this payment. Your order is not complete until payment is received.</p>            
  		<form method='post' action="https://secure.authorize.net/gateway/transact.dll">
			<input type='hidden' name="x_login" value="<?php echo $api_login_id?>" />
				<input type='hidden' name="x_fp_hash" value="<?php echo $fingerprint?>" />
				<input type='hidden' name="x_invoice_num" value="<?php echo $fp_sequence?>"/>
				<input type='hidden' name="x_amount" value="<?php echo $amount?>" />       
                <?php if ($formEntry["First Name"]=="Test" || $formEntry["Last Name"]=="Test"){ ?>
                	 <INPUT TYPE="HIDDEN" NAME="x_test_request" VALUE="TRUE">
                  <?php } ?>       
				<input type='hidden' name=" x_relay_url" value="<?php echo get_bloginfo('url') . $_SERVER['REQUEST_URI'];?>"/>
				<input type="hidden" name="x_logo_url" value="https://secure.authorize.net/mgraphics/logo_1303519_01.png">
				<input type='hidden' name="x_fp_timestamp" value="<?php echo $fp_timestamp?>" />
				<input type='hidden' name="x_fp_sequence" value="<?php echo $fp_sequence?>" />
				<input type='hidden' name="x_version" value="3.1">
				<input type='hidden' name="x_show_form" value="payment_form">				
				<input type='hidden' name="x_method" value="cc">                                                            
				<input type='submit' style="float:left; cursor:pointer; margin-right:1em;" value="Make Payment »">
				<?php if ($formEntry["Description"]){ ?>
					<input type='hidden' name="x_description" value="<?php echo $formEntry["Description"];?>"/>
				 <?php } ?>
                 <?php if ($formEntry["entryID"]){ ?>
					<input type='hidden' name="x_cust_id" value="<?php echo $formEntry["entryID"];?>"/>
				 <?php } ?>
				 <?php if ($formEntry["First Name"]){ ?>
					<input type='hidden' name="x_first_name" value="<?php echo $formEntry["First Name"];?>"/>
				 <?php } ?>
				 <?php if ($formEntry["Last Name"]){ ?>
					<input type='hidden' name="x_last_name" value="<?php echo $formEntry["Last Name"];?>"/>
				  <?php } ?>
				 <?php if ($formEntry["Email"]){ ?>
					<input type='hidden' name="x_email" value="<?php echo $formEntry["Email"];?>"/>
                    <input type='hidden' name="x_email_customer " value="TRUE">
                    <input type='hidden' name="x_header_email_receipt " value="<strong>Order Confirmation</strong><br><br><?php echo $formEntry["Description"];?><br><br>">
				 <?php } ?>
                 <?php if ($formEntry["Company"]){ ?>
                  <input type='hidden' name="x_company" value="<?php echo $formEntry["Company"]?>" />
                  <?php } ?>
				 <?php if ($formEntry["Address"]){ ?>
					<input type='hidden' name="x_address" value="<?php echo $formEntry["Address"];?>"/>
				 <?php } ?>
				 <?php if ($formEntry["City"]){ ?>    
					<input type='hidden' name="x_city" value="<?php echo $formEntry["City"];?>"/>
				 <?php } ?>
				 <?php if ($formEntry["State"]){ ?>
					<input type='hidden' name="x_state" value="<?php echo $formEntry["State"];?>"/>
				 <?php } ?>
				 <?php if ($formEntry["Zip"]){ ?>
					<input type='hidden' name="x_zip" value="<?php echo $formEntry["Zip"];?>"/>
				 <?php } ?>
				 <?php if ($formEntry["Country"]){ ?>
					<input type='hidden' name="x_country" value="<?php echo $formEntry["Country"];?>"/>
				 <?php } ?>
			</form>
            <p class='cancel-link' style="padding:.5em"><a href="<?php echo(basename($_SERVER['REQUEST_URI'])."&cancel=true"); ?>">Cancel</a></p>
            
      <?php 
			
			$form = retrieve_gf_form($entryID);	
		} 
		//var_dump($form);
	} else if ($_GET['response'] && $_GET['custid']){ // if payment made
		$entryID = $_GET['custid'];
		$response = $_GET['response'];
		$transID = $_GET['transid'];
		$amount = $_GET['amount'];				
		update_gf_entry_paystatus($entryID, $response, $transID, $amount);
		$theEntry=retrieve_gf_entry($entryID);
		echo("<h3>".$theEntry['Description']."</h3>");
		
		if($response==1){
			echo ("<p>Thank you. Payment has been received.</p>");					
			echo ("<p>".$theEntry['First Name']." ".$theEntry['Last Name']."<br>".$theEntry['Email']."<br>Confirmation Number: ".$theEntry['entryID']."<br>Total: $".$theEntry['Total']."<br>Date: ". date('m/d/Y') ."</p><p><button onclick='window.print()'>Print this page</button></p>");
			
		} else if ($response==2){
			echo ("<p>We're sorry, this transaction has been declined. Please contact your credit card company.</p>
			<p><a href='http://eduiconf.org/vfh-payment-page/?entry_id=". $entryID."'>Retry payment »</a></p>");
		} else if ($response==3){
			echo ("<p>There was an error processing this transaction.</p>");
		}
	} else { ?>
				<p>We're sorry, there's been an error with your form. Please contact <a href="mailto:vafh-web@virginia.edu">vafh-web@virginia.edu</a> for assistance.</p>                	
	<?php } ?>
            <p>&nbsp;</p>
			</div>
<?php
}

// Gravity Forms Custom Functions 
function retrieve_gf_entry($entry) {
	
	$entry_id = $entry;
	$the_entry = GFAPI::get_entry($entry_id);
	$form_id = $the_entry["form_id"];
	$form = GFAPI::get_form($form_id);	
	$formTitle = $form["title"];
	foreach ($form["fields"] as $field) {
		$fieldlabel = strtolower($field["label"]);
		if ($fieldlabel == "total"){
                $totalID = $field["id"];
				$total = $the_entry[$totalID];
		}
		if ($fieldlabel == "name"){
                $nameID = $field["id"];
				$firstNameID = $nameID . ".3";
				$lastNameID = $nameID . ".6";
		}
		//if (strtolower($field["label"]) == "address"){
		if (strpos($fieldlabel,'address') !== false && strpos($fieldlabel,'email') == false){
                $addressID = $field["id"];
				$streetID = $addressID . ".1";
				$cityID = $addressID . ".3";
				$stateID = $addressID . ".4";
				$zipID = $addressID . ".5";
				$countryID = $addressID . ".6";
		}
		if ($fieldlabel == "email"){
                $emailID = $field["id"];
		}	
		if ($fieldlabel == "payment status"){
                $statusID = $field["id"];
				$paymentstatus = $the_entry[$statusID];
		}	
		if ($fieldlabel == "company/institution"){
                $companyID = $field["id"];
		}	
    } 
	
	
	//$entry_dump = print_r($the_entry);
	
	//$entry_dump = print_r($values);
	$return = array ("First Name"=>$the_entry[$firstNameID], "Last Name"=>$the_entry[$lastNameID], "Total"=>$the_entry[$totalID], "Email"=>$the_entry[$emailID], "Address"=>$the_entry[$streetID], "City"=>$the_entry[$cityID], "State"=>$the_entry[$stateID], "Zip"=>$the_entry[$zipID], "Country"=>$the_entry[$countryID], "Description"=>$formTitle, "entryID"=>$entry_id, "paymentstatus"=>$paymentstatus, "Company"=>$the_entry[$companyID]); 
	
	return $return; 
	
}

function update_gf_entry_paystatus($entry, $response, $transid, $amount) {	
	$entry_id = $entry;
	$the_entry = GFAPI::get_entry($entry_id);
	$form_id = $the_entry["form_id"];
	$form = GFAPI::get_form($form_id);	
	
	date_default_timezone_set('America/New_York');
	
	foreach ($form["fields"] as $field) {
		if (strtolower($field["label"]) == "payment status"){
                $statusID = $field["id"];				
		}
		if (strtolower($field["label"]) == "payment date"){
                $dateID = $field["id"];				
		}
		if (strtolower($field["label"]) == "payment amount"){
                $amountID = $field["id"];				
		}
		if (strtolower($field["label"]) == "transaction id"){
                $transFieldID = $field["id"];				
		}
		
		if($response==1){
			$the_entry[$statusID] = 'Approved';
			$the_entry[$dateID] = date("m/d/Y H:i");
			$the_entry[$amountID] = $amount;
			$the_entry[$transFieldID] = $transid;
		} else if ($response==2){
			$the_entry[$statusID] = 'Declined';
			$the_entry[$dateID] = date("m/d/Y H:i");
			$the_entry[$transFieldID] = $transid;
		} else if ($response==3){
			$the_entry[$statusID] = 'Error';
			$the_entry[$dateID] = date("m/d/Y H:i");
			$the_entry[$transFieldID] = $transid;
		} else if ($response==4){
			$the_entry[$statusID] = 'Held';
			$the_entry[$dateID] = date("m/d/Y H:i");
			$the_entry[$transFieldID] = $transid;
		} 
    } 
	
	
	$success = GFAPI::update_entry($the_entry);
	GFCommon::send_form_submission_notifications($form, $the_entry);
	
	return $success; 
	
}

function retrieve_gf_form($entry) {	
	$entry_id = $entry;
	$the_entry = GFAPI::get_entry($entry_id);
	$form_id = $the_entry["form_id"];
	$form = GFAPI::get_form($form_id);	
	
	return $form; 
}

genesis();
?>