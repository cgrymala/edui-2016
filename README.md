# edUI 2016

## Version 0.1

This is the official WordPress theme for the 2016 [edUI Conference website](http://eduiconf.org/).

## Base

This theme is a child theme of the [Genesis Framework](http://StudioPress.com/) from StudioPress.

## Contributors

* Designer: John Rhea
* Developer: Curtiss Grymala and Trey Mitchell

## Installation

Before installing and activating this theme, it's recommended that you install and activate the [Image Widget]() plugin, as it can be extremely useful in the setup of the front page.

Also, this theme uses the [Views]() plugin to set up quite a few layouts, so that should be installed and licensed (license key available upon request).

1. Upload the theme folder to `/wp-content/themes/`
1. Activate the theme
1. Create a new page to act as the front page. The Featured Image for that page will be used as the main Hero Image on the front page; the content will be used as the caption for that hero image.
1. Go to Settings -> Reading and set that new page to act as the front page; set the Blog page to act as the posts page.
1. Create a new menu to hold the links to the previous conference sites.
1. Make sure the primary navigation menu is set up
1. Go to Appearance -> Header and set the VFH logo as the header image
1. Go to Appearance -> Widgets and set up the following widget areas:
    1. Footer Full-Width - This is where the contact information goes (`<a href="mailto:info@eduiconf.org">info@eduiconf.org</a> | 434-260-edUI`)
    1. Footer 1 - This is the copyright (Text widget), the Past Conferences custom menu, and the link to the Code of Conduct (Text widget) go.
    1. Footer 2 - This is where the website credits go (`<small>Web Design & Development</small> by John Rhea, Curtiss Grymala & Trey Mitchell`)
    1. [Front Page] Widget Area 1 - This is where the appropriate View for featured speakers should be placed
    1. [Front Page] Widget Area 2 - This is where the appropriate View for the featured blog posts should be placed
    1. [Front Page] Widget Area 3 - This is where an Image Widget with the main Venues feature image should be placed with a caption that describes the venue information
    1. [Front Page] Widget Area 4 - This is where the sponsor information should go. It's recommended to use a blank Text widget with "Sponsors" as the title, then use Image Widgets for each sponsor logo (using the "medium" image size). Another Text widget can then go below the logos to explain how to contact edUi about sponsoring.
    1. [Front Page] Widget Area 5 and [Front Page] Widget Area 6 - currently, these aren't used for anything, but, they're available if they're needed.
    1. Header Right - an Image Widget for the edUi logo and a Custom Menu widget for the primary navigation
    1. Primary Sidebar and Secondary Sidebar - not currently used for anything
