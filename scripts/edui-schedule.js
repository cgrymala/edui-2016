;/**
 * The JavaScript functions and definitions for the 
 * 		edUi Schedule
 */
jQuery( function( $ ) {
	/**
	 * @var int minuteHeight the vertical height that each minute of the schedule should take up
	 */
	var minuteHeight = 0;
	/**
	 * @var object MonthNames an associative object that associates the 0-based month numbers with full-text names
	 */
	var MonthNames = {
		0 : 'January', 
		1 : 'February', 
		2 : 'March', 
		3 : 'April', 
		4 : 'May', 
		5 : 'June', 
		6 : 'July', 
		7 : 'August', 
		8 : 'September', 
		9 : 'October', 
		10 : 'November', 
		11 : 'December'
	};
	/**
	 * @var int tzCorrect the amount of minutes to add to date/times, since the timezone seems to be off in ACF fields
	 */
	var tzCorrect = 0;
	
	/**
	 * Add a class of js-enabled to the body for styling purposes
	 */
	$( 'body' ).addClass( 'js-enabled' );
	
	/**
	 * Iterates through elements on the page to adjust the height of each element
	 * 		and to add breaks and spacers where necessary
	 * Un-does itself on mobile (as based on the .hide-from-mobile CSS class
	 */
	var doSessionHeight = function() {
		if ( $( '.hide-from-mobile' ).css( 'display' ) == 'none' ) {
			$( '[data-duration]' ).each( function() {
				$( this ).find( '> a' ).css( 'height', 'auto' );
			} );
			$( '.schedule-block .column' ).css( 'height', 'auto' );
			$( '.schedule-block .spacer' ).remove();
			
			/**
			 * Add accordion show/hide functionality to the individual time-blocks on mobile
			 */
			$( '.time-block-header' ).on( 'click', function() {
				$( this ).next( '.schedule-block' ).toggle();
			} );
			return;
		}
		$( '.time-block-header' ).off( 'click' );
		$( '.schedule-block:hidden' ).show();
		$( '[data-duration]' ).each( function() { 
			var dur = $( this ).attr( 'data-duration' ) * 1;
			var h = $( this ).find( '> a' ).outerHeight();
			var minH = ( h / dur );
			
			if ( minH > minuteHeight ) {
				minuteHeight = minH;
			}
		} );
		$( '[data-duration]' ).each( function() { 
			if ( $( this ).hasClass( 'conference-wide' ) ) {
				return;
			}
			var dur = $( this ).attr( 'data-duration' ) * 1;
			$( this ).find( '> a' ).outerHeight( ( minuteHeight * dur ) );
		} );
		if ( document.querySelectorAll( '.spacer' ).length >= 1 ) {
			return;
		}
		$( '.column' ).each( function() {
			var $firstSession = $( this ).find( '[data-duration]:first' );
			var $sib = $( this ).next().find( '[data-duration]:first' );
			if ( $sib.length <= 0 ) {
				$sib = $( this ).prev().find( '[data-duration]:first' );
				if ( $sib.length <= 0 ) {
					return;
				}
			}
			var sessionDate = new Date();
			sessionDate.setTime( Date.parse( $firstSession.attr( 'data-start' ) ) );
			sessionDate = dateCorrect( sessionDate );
			var sibDate = new Date();
			sibDate.setTime( Date.parse( $sib.attr( 'data-start' ) ) );
			sibDate = dateCorrect( sibDate );
			
			if ( sessionDate.getTime() > sibDate.getTime() ) {
				var minDiff = sessionDate.getTime() - sibDate.getTime();
				minDiff = ( ( minDiff / 1000 ) / 60 );
				$firstSession.parent().find( '.block-header' ).after( '<article class="session spacer" data-duration="' + minDiff + '" data-start="' + sibDate.toString() + '" style="height: ' + ( minuteHeight * minDiff ) + 'px"><a>&nbsp;</a></article>' );
			}
		} );
		$( '.column' ).each( function() {
			if ( $( this ).find( '[data-duration]' ).length > 1 ) {
				$( this ).find( '[data-duration]' ).each( function() {
					var $sib = $( this ).next();
					if ( $sib.length <= 0 ) {
						return;
					}
					var sessionEnd = new Date();
					sessionEnd.setTime( Date.parse( $( this ).attr( 'data-start' ) ) );
					sessionEnd.setTime( sessionEnd.getTime() + ( $( this ).attr( 'data-duration' ) * 60 * 1000 ) );
					sessionEnd = dateCorrect( sessionEnd );
					var sibStart = new Date();
					sibStart.setTime( Date.parse( $sib.attr( 'data-start' ) ) );
					sibStart = dateCorrect( sibStart );
					
					if ( sibStart.getTime() > sessionEnd.getTime() ) {
						var minDiff = sibStart.getTime() - sessionEnd.getTime();
						minDiff = ( ( minDiff / 1000 ) / 60 );
						$sib.before( '<article class="session spacer" data-duration="' + minDiff + '" data-start="' + sessionEnd.toString() + '" style="height:' + ( minuteHeight * minDiff ) + 'px"><a><header><strong class="session-type">Break</strong></header><p class="session-date">' + eduiParseDate( sessionEnd, 'full' ) + ' - ' + eduiParseDate( sibStart, 'time' ) + '</p></a></article>' );
					}
				} );
			}
		} );
	};
	/**
	 * Ensure that the blocks are readjusted when the browser is re-sized
	 */
	$( window ).on( 'resize', doSessionHeight );
	
	/**
	 * @var Date curDate the current date, as represented by a JS Date object
	 */
	var curDate = new Date();
	/**
	 * @var int curYear the 4-digit year of the current date
	 */
	var curYear = curDate.getFullYear();
	/**
	 * @var int curMonth the 0-based numeric representation of the current month
	 */
	var curMonth = curDate.getMonth();
	/**
	 * @var int curDay the current day of the month
	 */
	var curDay = curDate.getDate();

	var scheduleHash = window.location.hash;
	var dayBlocks = {
		'#edui-schedule-day-1' : $( '#edui-schedule-day-1' ).find( '.day-schedule-wrapper' ), 
		'#edui-schedule-day-2' : $( '#edui-schedule-day-2' ).find( '.day-schedule-wrapper' ), 
		'#edui-schedule-day-3' : $( '#edui-schedule-day-3' ).find( '.day-schedule-wrapper' )
	};
	if ( scheduleHash in dayBlocks ) {
		
		$( '.day-schedule-wrapper' ).hide();
		$( dayBlocks[scheduleHash] ).show();
		
	} else {
	
		/**
		 * Iterate through each day in the schedule, and hide anything that doesn't match today's date
		 * If the day hash is set, we open the appropriate day instead of opening the current day
		 */
		$( '.day-schedule-wrapper' ).each( function() {
			var scheduleDate = new Date( $( this ).closest( '.schedule-day' ).attr( 'data-date' ) );
			scheduleDate = dateCorrect( scheduleDate );
			var scheduleDay = scheduleDate.getFullYear() + '-' + scheduleDate.getMonth() + '-' + scheduleDate.getDate();
			var curDayDate = curYear + '-' + curMonth + '-' + curDay;
			if ( scheduleDay != curDayDate ) {
				$( this ).hide();
			}
		} );
		
		/**
		 * If today's date does not match any of the days on the schedule, show the first day
		 */
		if ( $( '.day-schedule-wrapper:visible' ).length == 0 ) {
			$( '.day-schedule-wrapper' ).first().show();
		}
		
	}
	
	/**
	 * Add accordion show/hide functionality to the day blocks
	 */
	$( '.schedule-day .day-header' ).on( 'click', function() {
		if ( $( this ).closest( '.schedule-day' ).find( '.day-schedule-wrapper:visible' ).length >= 1 ) {
			$( this ).closest( '.schedule-day' ).find( '.day-schedule-wrapper' ).hide();
			return false;
		}
		$( '.day-schedule-wrapper' ).hide();
		$( this ).closest( '.schedule-day' ).find( '.day-schedule-wrapper' ).show();
		var eduiHash = $( this ).find( 'a' ).attr( 'href' );
		
		if( history.pushState ) {
			history.pushState( null, null, eduiHash );
		}
		else {
			location.hash = eduiHash;
		}

		doSessionHeight();
		var offSet = $( this ).offset().top;
		if ( document.querySelectorAll( '#wpadminbar' ).length >= 1 ) {
			offSet -= 50;
		} else {
			offSet -= 18;
		}
		$( 'html, body' ).animate({
			scrollTop: offSet
		}, 500 );
		return false;
	} );
	
	/**
	 * Ensure that the block heights are all adjusted properly on page load
	 */
	doSessionHeight();
	
	/**
	 * Attempt to parse JS date object into a readable date/time similar to the PHP format 'F j, Y g:i a'
	 */
	function eduiParseDate( date, type ) {
		var dateParts = {};
		dateParts.year = date.getFullYear();
		dateParts.month = MonthNames[date.getMonth()];
		dateParts.date = date.getDate();
		dateParts.hour = date.getHours();
		dateParts.minute = date.getMinutes();
		
		if ( dateParts.hour > 11 ) {
			dateParts.ap = 'pm';
		} else {
			dateParts.ap = 'am';
		}
		
		if ( dateParts.hour == 0 ) {
			dateParts.hour = 12;
		} else if ( dateParts.hour > 11 ) {
			dateParts.hour -= 12;
		}
		
		if ( dateParts.minute < 10 ) {
			dateParts.minute = '0' + dateParts.minute;
		}
		
		dateParts.time = dateParts.hour + ':' + dateParts.minute + ' ' + dateParts.ap;
		
		if ( 'time' == type ) {
			return dateParts.time;
		} else {
			return dateParts.month + ' ' + dateParts.date + ', ' + dateParts.year + ' ' + dateParts.time;
		}
	}
	
	/**
	 * Attempt to fix the fact that the ACF date/time fields are stored as GMT even though 
	 * 		though already have the timezone offset (so, 2016-10-24 09:00:00 EDT is being 
	 * 		stored as 2016-10-24 09:00:00 GMT
	 * Adds the timezone offset onto the date
	 */
	function dateCorrect( date ) {
		if ( isNaN( date ) ) {
			return date;
		}
			
		if ( tzCorrect == 0 ) {
			var tz = date.getTimezoneOffset();
			tzCorrect = tz * 1;
		}
		
		date.setMinutes( date.getMinutes() + tzCorrect );
		return date;
	}
	
	/**
	 * Attempt to add current time awareness to the schedule, including a "now" button
	 */
	function getScheduleNow() {
		if ( document.querySelectorAll( '.schedule-nav' ).length <= 0 ) {
			addScheduleButtons();
		}
		
		var now = new Date();
		// Make sure the month is 2-digits
		var tmpMonth = now.getMonth();
		tmpMonth++;
		tmpMonth = '00' + tmpMonth;
		tmpMonth = tmpMonth.substr( -2, 2 );

		// Set the date that we'll be searching the HTML for
		var searchDate = now.getFullYear() + '-' + tmpMonth + '-' + now.getDate();
		$( '[data-start^="' + searchDate + '"]' ).each( function() {
			var blockDate = new Date();
			blockDate.setTime( Date.parse( $(this).attr( 'data-start' ) ) );
			blockDate = dateCorrect( blockDate );
			if ( blockDate <= now ) {
				var blockEnd = new Date();
				blockEnd.setTime( blockDate.getTime() );
				blockEnd.setMinutes( blockEnd.getMinutes() + $( this ).attr( 'data-duration' ) );
				if ( blockEnd >= now ) {
					$( '#now' ).remove();
					$( this ).prepend( '<span id="now"></span>' );
				}
			}
			
			/**
			 * The 'now' button does not exist yet
			 */
			if ( document.querySelectorAll( '.now-button' ).length <= 0 ) {
				$( '.schedule-nav' ).addClass( 'has-now' ).find( '.top-button' ).after( '<a href="#now" class="now-button">Now</a>' );
				
				$( '.now-button' ).on( 'click', function() {
					var eduiHash = $( this ).attr( 'href' );
					
					if( history.pushState ) {
						history.pushState( null, null, eduiHash );
					}
					else {
						location.hash = eduiHash;
					}
					
					var offSet = $( '#now' ).offset().top;
					if ( document.querySelectorAll( '#wpadminbar' ).length >= 1 ) {
						offSet -= 50;
					} else {
						offSet -= 18;
					}
					$( 'html, body' ).animate({
						scrollTop: offSet
					}, 500 );
					return false;
				} );
			}
		} );
		
		nowT = setTimeout( getScheduleNow, ( 5 * 60 * 1000 ) );
	}
	
	function addScheduleButtons() { 
		$( 'body' ).append( '<nav class="schedule-nav clearfix"><a href="#schedule-top" class="top-button">Top</a><a href="#schedule-bottom" class="bottom-button">Bottom</a></nav>' );
		
		$( 'main.content' ).append( '<a id="schedule-bottom"/>' );
		$( 'main.content' ).prepend( '<a id="schedule-top"/>' );
		$( '.top-button, .bottom-button' ).on( 'click', function() {
			var eduiHash = $( this ).attr( 'href' );
			
			if( history.pushState ) {
				history.pushState( null, null, eduiHash );
			}
			else {
				location.hash = eduiHash;
			}
			
			var offSet = $( $( this ).attr( 'href' ) ).offset().top;
			if ( document.querySelectorAll( '#wpadminbar' ).length >= 1 ) {
				offSet -= 50;
			} else {
				offSet -= 18;
			}
			$( 'html, body' ).animate({
				scrollTop: offSet
			}, 500 );
			return false;
		} );
	}
	
	var nowT = null;
	getScheduleNow();
} );