<?php
/**
 * Template Name: edUi Schedule
 *
 * @package WordPress
 * @subpackage edUi 2016
 * @since 0.1.6
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

if ( ! class_exists( 'edUi_Schedule_Template' ) ) {
	class edUi_Schedule_Template {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $version = '0.1.10';
		
		/**
		 * Holds the list of all timestamps associated with sessions
		 * 
		 * @since  0.1
		 * @access private
		 * @var    array
		 */
		private $session_times = array();
		
		/**
		 * Holds the number of the current day being processed
		 *
		 * @since  0.1
		 * @access private
		 * @var    int
		 */
		private $schedule_day = 0;
		
		/**
		 * Holds an array of conference-wide venues
		 * 
		 * @since  0.1
		 * @access private
		 * @var    array
		 */
		private $conference_wide_venues = array( 'Jefferson Theater', 'LiveArts: Roof Deck', 'Various', '' );
		
		/**
		 * Holds an array of workshop venues
		 * 
		 * @since  0.1
		 * @access private
		 * @var    array
		 */
		private $workshop_venues = array( 'CitySpace' );
		
		/**
		 * Holds the class instance.
		 *
		 * @since	1.0.0
		 * @access	private
		 * @var		edui2016
		 */
		private static $instance;
		
		/**
		 * Holds the IDs of the Views/Content Templates used for the schedule
		 * 
		 * @since  0.1
		 * @access private
		 * @var    array
		 */
		private $view_ids = array();
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   0.1
		 * @return	edui2016
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the edUi Schedule object
		 */
		function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
			
			$this->set_view_ids();
		}
		
		/**
		 * Retrieve the IDs of our schedule Views by slug
		 */
		function set_view_ids() {
			$views = array( 'schedule-conference-wide-query', 'schedule-workshop-query', 'schedule-concurrent-sessions-query', 'schedule-block-template' );
			$view_posts = get_posts( array( 
				'post_type' => array( 'view', 'view-template' ), 
				'post_status' => 'publish', 
				'posts_per_page' => -1, 
				'post_name__in' => $views
			) );
			
			foreach ( $view_posts as $p ) { 
				switch( $p->post_name ) {
					case 'schedule-conference-wide-query' : 
						$this->view_ids['conference-wide'] = $p->ID;
						break;
					case 'schedule-workshop-query' : 
						$this->view_ids['workshops'] = $p->ID;
						break;
					case 'schedule-concurrent-sessions-query' : 
						$this->view_ids['sessions'] = $p->ID;
						break;
					default : 
						$this->view_ids['block_template'] = $p->ID;
						break;
				}
			}
			
			wp_reset_postdata();
			wp_reset_query();
		}
		
		/**
		 * Output the schedule
		 */
		public function do_schedule() {
			printf( '<div class="schedule-intro">%s</div>', apply_filters( 'the_content', get_the_content() ) );
			
			foreach ( array( 0, 1, 2, 3 ) as $d ) {
				$this->edui_schedule_day = $d;
				call_user_func( array( $this, 'do_schedule_day_' . $d ) );
			}
		}
		
		/**
		 * Output a day of the schedule
		 */
		function do_schedule_day( $start, $end, $blocks ) {
			$this->session_times = array();
			$conference_wide_events = $this->get_conference_wide_events( $start, $end );
			$workshops = $this->get_workshop_events( $start, $end );
			$sessions = $this->get_session_events( $start, $end );
			$this->session_times = array_unique( $this->session_times );
			
			if ( empty( $conference_wide_events ) && empty( $workshops ) && empty( $sessions ) )
				return;
			
			echo '
			<div class="schedule-day clearfix full-width" data-date="' . date( 'Y-m-d', $start ) . '" id="edui-schedule-day-' . $this->edui_schedule_day . '">
				<header class="day-header">
					<h1><a href="#edui-schedule-day-' . $this->edui_schedule_day . '">' . date( 'F j, Y', $start ) . '</a></h1>
				</header>
				<div class="day-schedule-wrapper">';
			
			sort( $this->session_times, SORT_NUMERIC );
			$current_block = array( 'start' => 0, 'end' => 0 );
			$current_session_time = 0;
			$meat = '';
			$innards = array();
			
			foreach ( $this->session_times as $time ) {
				if ( 0 == $current_session_time ) {
					$current_session_time = $time;
				}
				foreach ( $blocks as $o => $c ) {
					if ( $time >= $o && $time <= $c ) {
						if ( $current_block['start'] != $o && ! empty( $innards ) ) {
							print( '<div class="time-block-header">' . date( 'F j, Y g:i a', $current_session_time ) . ' - ' . date( 'g:i a', $current_block['end'] ) . '</div>' );
							$this->output_wrapper_block( $innards );
							$innards = array();
							$current_session_time = $o;
						}
						$current_block = array( 'start' => $o, 'end' => $c );
					}
				}
				
				if ( array_key_exists( $time, $conference_wide_events ) ) {
					foreach ( $conference_wide_events[$time] as $location => $post ) {
						$type = get_the_terms( $post->ID, 'session-type');
						if ( is_array( $type ) )
							$type = array_pop( $type );
						if ( is_object( $type ) )
							$type = $type->slug;
						if ( ! array_key_exists( 'cw', $innards ) ) {
							$innards['cw'] = '';
						}
						if ( $current_block['end'] < get_field( 'date-time', $post->ID, false ) + ( get_field( 'duration', $post->ID, false ) * 60 ) ) {
							$current_block['end'] = get_field( 'date-time', $post->ID, false ) + ( get_field( 'duration', $post->ID, false ) * 60 );
						}
						$innards['cw'] .= sprintf( '<article class="session conference-wide full-width clearfix %s %s" data-duration="%d" data-start="%s">', $type, sanitize_title( $location ), get_field( 'duration', $post->ID, false ), date( 'c', get_field( 'date-time', $post->ID, false ) ) );
						$innards['cw'] .= $this->do_session_block( $post );
						$innards['cw'] .= '</article>';
					}
				} else {
					if ( array_key_exists( $time, $sessions ) ) {
						foreach ( $sessions[$time] as $location => $post ) {
							$type = get_the_terms( $post->ID, 'session-type');
							if ( is_array( $type ) )
								$type = array_pop( $type );
							if ( is_object( $type ) )
								$type = $type->slug;
							
							if ( ! array_key_exists( $location, $innards ) ) {
								$innards[$location] = '';
							}
							if ( $current_block['end'] < get_field( 'date-time', $post->ID, false ) + ( get_field( 'duration', $post->ID, false ) * 60 ) ) {
								$current_block['end'] = get_field( 'date-time', $post->ID, false ) + ( get_field( 'duration', $post->ID, false ) * 60 );
							}
							$innards[$location] .= sprintf( '<article class="session concurrent-session %s %s" data-duration="%d" data-start="%s">', $type, sanitize_title( $location ), get_field( 'duration', $post->ID, false ), date( 'c', get_field( 'date-time', $post->ID, false ) ) );
							$innards[$location] .= $this->do_session_block( $post );
							$innards[$location] .= '</article>';
						}
					}
					if ( array_key_exists( $time, $workshops ) ) {
						foreach ( $workshops[$time] as $location => $post ) {
							$type = get_the_terms( $post->ID, 'session-type');
							if ( is_array( $type ) )
								$type = array_pop( $type );
							if ( is_object( $type ) )
								$type = $type->slug;
							
							if ( ! array_key_exists( $location, $innards ) ) {
								$innards[$location] = '';
							}
							if ( $current_block['end'] < get_field( 'date-time', $post->ID, false ) + ( get_field( 'duration', $post->ID, false ) * 60 ) ) {
								$current_block['end'] = get_field( 'date-time', $post->ID, false ) + ( get_field( 'duration', $post->ID, false ) * 60 );
							}
							$innards[$location] .= sprintf( '<article class="session workshop-block %s %s" data-duration="%d" data-start="%s">', $type, sanitize_title( $location ), get_field( 'duration', $post->ID, false ), date( 'c', get_field( 'date-time', $post->ID, false ) ) );
							$innards[$location] .= $this->do_session_block( $post );
							$innards[$location] .= '</article>';
						}
					}
				}
			}
			
			if ( ! empty( $innards ) ) {
				print( '<div class="time-block-header">' . date( 'F j, Y g:i a', $current_session_time ) . ' - ' . date( 'g:i a', $current_block['end'] ) . '</div>' );
				$this->output_wrapper_block( $innards );
				$current_session_time = 0;
			}
			
			echo '</div></div>';
		}
		
		/**
		 * Output the pre-conference day's schedule
		 */
		function do_schedule_day_0() {
			$start = strtotime( '2017-09-24 00:00:00' );
			$end = strtotime( '2017-09-24 23:59:59' );
			$blocks = array(
				strtotime( '2017-09-24 00:00:00' ) => strtotime( '2017-09-24 23:59:59' )
			);
			
			$this->do_schedule_day( $start, $end, $blocks );
		}
		
		/**
		 * Output the first day's schedule
		 */
		function do_schedule_day_1() {
			$start = strtotime( '2017-09-25 00:00:00' );
			$end = strtotime( '2017-09-25 23:59:59' );
			$blocks = array(
				strtotime( '2017-09-25 00:00:00' ) => strtotime( '2017-09-25 10:15:00' ), 
				strtotime( '2017-09-25 10:15:00' ) => strtotime( '2017-09-25 12:00:00' ), 
				strtotime( '2017-09-25 12:00:00' ) => strtotime( '2017-09-25 13:30:00' ), 
				strtotime( '2017-09-25 13:30:00' ) => strtotime( '2017-09-25 17:15:00' ), 
				strtotime( '2017-09-25 13:30:00' ) => strtotime( '2017-09-25 16:00:00' ),
				strtotime( '2017-09-25 16:00:00' ) => strtotime( '2017-09-25 23:59:59' )
			);
			
			$this->do_schedule_day( $start, $end, $blocks );
		}
		
		/**
		 * Output the second day's schedule
		 */
		function do_schedule_day_2() {
			$start = strtotime( '2017-09-26 00:00:00' );
			$end = strtotime( '2017-09-26 23:59:59' );
			$blocks = array(
				strtotime( '2017-09-26 00:00:00' ) => strtotime( '2017-09-26 10:15:00' ), 
				strtotime( '2017-09-26 10:15:00' ) => strtotime( '2017-09-26 12:00:00' ), 
				strtotime( '2017-09-26 12:00:00' ) => strtotime( '2017-09-26 13:30:00' ), 
				strtotime( '2017-09-26 13:30:00' ) => strtotime( '2017-09-26 15:00:00' ), 
				strtotime( '2017-09-26 15:00:00' ) => strtotime( '2017-09-26 15:30:00' ), 
				strtotime( '2017-09-26 15:30:00' ) => strtotime( '2017-09-26 17:15:00' ),
				strtotime( '2017-09-26 17:15:00' ) => strtotime( '2017-09-26 23:59:59' )
			);
			
			$this->do_schedule_day( $start, $end, $blocks );
		}
		
		/**
		 * Output the third day's schedule
		 */
		function do_schedule_day_3() {
			$start = strtotime( '2017-09-27 00:00:00' );
			$end = strtotime( '2017-09-27 23:59:59' );
			$blocks = array(
				strtotime( '2017-09-27 00:00:00' ) => strtotime( '2017-09-27 09:00:00' ), 
				strtotime( '2017-09-27 09:00:00' ) => strtotime( '2017-09-27 11:00:00' ), 
				strtotime( '2017-09-27 11:00:00' ) => strtotime( '2017-09-27 23:59:59' )
			);
			
			$this->do_schedule_day( $start, $end, $blocks );
		}
		
		/**
		 * Retrieve the list of conference-wide events occurring on the specified day
		 */
		function get_conference_wide_events( $start, $end ) {
			$unsorted = array();
			// First, retrieve all of our conference-wide events
			/*$args = array(
				'post_type' => 'sessions', 
				'posts_per_page' => -1, 
				'orderby'   => 'meta_value_num', 
				'meta_key'  => 'date-time', 
				'meta_query' => array( array( 
					'key'   => 'date-time', 
					'value' => array( $start, $end ), 
					'type'       => 'numeric', 
					'compare'    => 'BETWEEN', 
				), array(
					'key'   => 'location', 
					'value' => $this->conference_wide_venues, 
					'compare'    => 'IN', 
				) ), 
			);
			$unsorted = get_posts( $args );*/
			
			$unsorted = get_view_query_results( $this->view_ids['conference-wide'], null, null, array( 'wpvblockstart' => $start, 'wpvblockend' => $end ) );
			
			foreach( $unsorted as $c ) {
				$date = get_field( 'date-time', $c->ID, false );
				$location = get_field( 'location', $c->ID, false );
				$this->session_times[] = $date;
				
				$conference_wide_events[$date][$location] = clone $c;
			}
			
			return $conference_wide_events;
		}
		
		/**
		 * Retrieve an array of workshop events happening on the specified day
		 */
		function get_workshop_events( $start, $end ) {
			$unsorted = array();
			// Next, retrieve all of our workshops
			/*$args = array(
				'post_type' => 'sessions', 
				'posts_per_page' => -1, 
				'orderby'   => 'meta_value_num', 
				'meta_key'  => 'date-time', 
				'meta_query' => array( array( 
					'key'     => 'date-time', 
					'value'   => array( $start, $end ), 
					'type'    => 'numeric', 
					'compare' => 'BETWEEN', 
				), array(
					'key'     => 'location', 
					'value'   => $this->workshop_venues, 
					'compare' => 'IN', 
				) )
			);
			$unsorted = get_posts( $args );*/
			
			$unsorted = get_view_query_results( $this->view_ids['workshops'], null, null, array( 'wpvblockstart' => $start, 'wpvblockend' => $end ) );
			
			$workshops = array();
			foreach ( $unsorted as $w ) {
				$date = get_field( 'date-time', $w->ID, false );
				$location = get_field( 'location', $w->ID, false );
				$this->session_times[] = $date;
				
				$workshops[$date][$location] = clone $w;
			}
			
			return $workshops;
		}
		
		/**
		 * Retrieve all session events (non-workshops and non-conference-wide)
		 */
		function get_session_events( $start, $end ) {
			$unsorted = array();
			// Finally, retrieve all other sessions
			/*$args = array(
				'post_type' => 'sessions', 
				'posts_per_page' => -1, 
				'orderby'   => 'meta_value_num', 
				'meta_key'  => 'date-time', 
				'meta_query' => array( array( 
					'key'   => 'date-time', 
					'value' => array( $start, $end ), 
					'type'       => 'numeric', 
					'compare'    => 'BETWEEN', 
				), array(
					'key'   => 'location', 
					'value' => array_merge( $this->conference_wide_venues, $this->workshop_venues ), 
					'compare'    => 'NOT IN', 
				) )
			);
			$unsorted = get_posts( $args );*/
			
			$unsorted = get_view_query_results( $this->view_ids['sessions'], null, null, array( 'wpvblockstart' => $start, 'wpvblockend' => $end ) );
			
			$sessions = array();
			foreach ( $unsorted as $session ) {
				$location = get_field( 'location', $session->ID, false );
				$date = get_field( 'date-time', $session->ID, false );
				
				$this->session_times[] = $date;
				
				$sessions[$date][$location] = clone $session;
			}
			
			// Sort all of our concurrent sessions in date, then location order
			foreach ( array_keys( $sessions ) as $date ) {
				ksort( $sessions[$date] );
			}
			
			return $sessions;
		}
		
		/**
		 * Wrap the sessions in a specific block in the appropriate HTML
		 */
		function output_wrapper_block( $innards ) {
			$meat = '';
			if ( count( $innards ) > 1 ) {
				/**
				 * Make an array of locations starting with the Workshop location, then the 
				 * 		mini-workshop location, the other concurrent session locations
				 */
				//$locations_in_order = array( 'Old Metropolitan', 'LiveArts: Gibson', 'LiveArts: Founders', 'LiveArts: Rehearsal A' );
				if ( 3 == $this->edui_schedule_day ) {
                	$locations_in_order = array('CitySpace', 'LiveArts: Gibson', 'LiveArts: Rehearsal A');
				} else {
					$locations_in_order = array( 'Old Metropolitan', 'LiveArts: Gibson', 'LiveArts: Founders', 'LiveArts: Rehearsal A' );
				}

				
				foreach ( $locations_in_order as $k => $l ) {
					switch ( $k ) {
						case 0 :
							if ( array_key_exists( $l, $innards ) ) {
								$meat .= '<div class="full-width clearfix"><div class="one-fourth first workshop-blocks cityspace-column column"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= $innards[$l];
								unset( $innards[$l] );
							} else {
								$meat .= '<div class="full-width clearfix"><div class="one-fourth first workshop-blocks cityspace-column column hide-from-mobile"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= '&nbsp;';
							}
							$meat .= '</div>';
							break;
						case 1 : 
							if ( array_key_exists( $l, $innards ) ) {
								$meat .= '<div class="three-fourths concurrent-blocks"><div class="one-third first gibson-column column"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= $innards[$l];
								unset( $innards[$l] );
							} else {
								$meat .= '<div class="three-fourths concurrent-blocks hide-from-mobile"><div class="one-third first gibson-column column"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= '&nbsp;';
							}
							$meat .= '</div>';
							break;
						case 2 : 
							if ( array_key_exists( $l, $innards ) ) {
								$meat .= '<div class="two-thirds"><div class="one-half first founders-column column"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= $innards[$l];
								unset( $innards[$l] );
							} else {
								$meat .= '<div class="two-thirds"><div class="one-half first founders-column column hide-from-mobile"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= '&nbsp;';
							}
							$meat .= '</div>';
							break;
						case 3 : 
							if ( array_key_exists( $l, $innards ) ) {
								$meat .= '<div class="one-half rehearsal-column column"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= $innards[$l];
								unset( $innards[$l] );
							} else {
								$meat .= '<div class="one-half rehearsal-column column hide-from-mobile"><header class="block-header"><h4>' . $l . '</h4></header>';
								$meat .= '&nbsp;';
							}
							$meat .= '</div></div></div></div>';
							break;
					}
				}
			} else {
				$meat = implode( "\n\r", $innards );
			}
			printf( '<div class="full-width schedule-block clearfix">%s</div>', $meat );
		}
		
		/**
		 * Put together the HTML for a specific session
		 */
		function do_session_block( $session ) {
			global $post;
			$post = $session;
			setup_postdata( $post );
			$sessioninfo = render_view_template( $this->view_ids['block_template'] );
			wp_reset_postdata();
			return $sessioninfo;
		}
		
		/**
		 * Enqueue the styles and scripts used for this template
		 */
		function enqueue_assets() {
			wp_enqueue_style( 'edui-schedule', get_stylesheet_directory_uri() . '/styles/edui-schedule.css', array( 'edui-2016' ), $this->version, 'all' );
			wp_enqueue_script( 'edui-schedule', get_stylesheet_directory_uri() . '/scripts/edui-schedule.js', array( 'jquery' ), $this->version, true );
		}
	}
}


/**
 * Instantiate an instance of the edui2016 class
 */
function eduiSchedule() {
	return edUi_Schedule_Template::instance();
}

global $eduiSchedule;
$eduiSchedule = eduiSchedule();
remove_all_actions( 'genesis_loop' );
add_action( 'genesis_loop', array( $eduiSchedule, 'do_schedule' ) );

genesis();